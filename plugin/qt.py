import asyncio
import json
import ssl
import time
import requests
import random
import string
import base64
from PyQt5 import QtWidgets, QtGui
from PyQt5.QtGui import QColor, QPixmap, QFont
from PyQt5.QtWidgets import (
    QTabWidget,
    QWidget,
    QMessageBox,
    QVBoxLayout,
    QLineEdit,
    QPushButton,
    QLabel,
    QHBoxLayout,
    QFormLayout,
    QListWidget,
    QListWidgetItem,
    QGroupBox,
    QProgressBar,
    QDialog,
)
from electrum.network import Network
from electrum.i18n import _
from electrum.gui.qt.util import WindowModalDialog
from electrum.plugin import BasePlugin, hook
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot, QObject, QThread, QTimer
from electrum.transaction import (
    Transaction,
    PartialTransaction,
    PartialTxInput,
    PartialTxOutput,
)
from nostr.event import Event, EncryptedDirectMessage
from nostr.relay_manager import RelayManager
from nostr.message_type import ClientMessageType
from nostr.filter import Filter, Filters
from nostr.key import PrivateKey
import threading
import subprocess
import os
import qrcode
from io import BytesIO
import ast
from pyqtspinner import WaitingSpinner


class SignalEmitter(QObject):
    register_output_signal = pyqtSignal(str, QWidget, object, bool)
    show_inputlist_signal = pyqtSignal(QWidget, QWidget, QLabel)
    close_dialog_signal = pyqtSignal()
    add_tab_signal = pyqtSignal(QTabWidget, QWidget, str)
    update_finalized_signal = pyqtSignal(QWidget, QWidget, QLabel)

class Plugin(BasePlugin):

    def __init__(self, parent, config, name):
        BasePlugin.__init__(self, parent, config, name)
        self.signal_emitter = SignalEmitter()
        self.address_text_fields = {}
        self.address = None
        self.private_key_hex = None
        self.output_list = []
        self.input_list = []
        self.peers = None
        self.my_pools = []
        self.other_pools = []
        self.pool = None
        self.pool_amount = None
        self.pool_fee_rate = None
        self.input_registration_widget = None
        self.other_pools_widget = None
        self.finalization_widget = None
        self.wallet = None
        self.reset_button = None
        self.spinner = None
        self.spinner_widget = None

    def requires_settings(self):
        return True

    def settings_widget(self, window):
        btn = QtWidgets.QPushButton(_("Settings"))
        btn.clicked.connect(lambda: self.settings_dialog(window))
        return btn

    def settings_dialog(self, window):
        d = WindowModalDialog(window, _("joinstr config"))
        self.d = d
        layout = QtWidgets.QVBoxLayout(d)

        settings = [
            {"label": _("Nostr Relay:"), "key": "nostr_relay"},
            {
                "label": _("Pool Denomination:"),
                "key": "pool_denomination",
                "validator": self.float_validator,
            },
            {
                "label": _("Number of peers:"),
                "key": "num_peers",
                "validator": self.integer_validator,
            },
        ]

        for setting in settings:
            label = QtWidgets.QLabel(setting["label"])
            edit = QtWidgets.QLineEdit()
            edit.setText(self.config.get(setting["key"], ""))
            edit.setFixedWidth(300)

            if "validator" in setting:
                validator = setting["validator"]()
                edit.setValidator(validator)

            setattr(self, f"{setting['key']}_edit", edit)
            layout.addWidget(label)
            layout.addWidget(edit)

        ok_button = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok, d)
        layout.addWidget(ok_button)
        ok_button.accepted.connect(
            lambda: self.ok_clicked(
                self.nostr_relay_edit.text(),
                self.pool_denomination_edit.text(),
                self.num_peers_edit.text(),
            )
        )
        d.exec_()

    def float_validator(self):
        return QtGui.QDoubleValidator(bottom=0.0, decimals=8)

    def integer_validator(self):
        return QtGui.QIntValidator(bottom=1)

    def ok_clicked(self, relay, pool_denomination, num_peers):

        self.config.set_key("nostr_relay", relay)
        self.config.set_key("pool_denomination", pool_denomination)
        self.config.set_key("num_peers", num_peers)
        self.d.accept()

    def close_dialog_slot(self):
        if self.dialog:
            self.dialog.accept()

    def connect_vpn(self, config):
        process = subprocess.Popen(
            ["sudo", "openvpn", "--config", config],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
        )
        vpn_connected = False

        for line in process.stdout:
            if "Initialization Sequence Completed" in line:
                response = requests.get("https://ifconfig.me")
                public_ip = response.text.strip()
                print(f"[joinstr plugin] Connected to VPN. Public IP: {public_ip}")
                vpn_connected = True
                break

        process.wait()

        if not vpn_connected:
            print("Error: VPN connection failed")
            print(process.stderr.read())

    def get_valid_pools(self, pools):
        """Sort the provided pools by descending timeout and filter out expired pools."""
        current_time = int(time.time())
        sorted_pools = sorted(pools, key=lambda x: x['timeout'], reverse=True)
        return list(filter(lambda x: x['timeout'] > current_time, sorted_pools))

    @hook
    def load_wallet(self, wallet, window):
        if not self.is_enabled():
            return

        config_url = "https://gitlab.com/invincible-privacy/joinstr/-/raw/main/config.vpn"
        response = requests.get(config_url)
        if response.status_code == 200:
            config_content = response.text
        else:
            print(
                f"Error: Unable to fetch VPN configuration. Status Code: {response.status_code}"
            )
            return

        config = "joinstr_config.vpn"
        config_path = os.path.expanduser(os.path.join("~", ".electrum", config))
        with open(config_path, "w") as config_file:
            config_file.write(config_content)

        connect_vpn_thread = threading.Thread(target=self.connect_vpn, args=(config_path,))
        connect_vpn_thread.daemon = True
        connect_vpn_thread.start()

        tab_widget = QTabWidget()
        tab_widget.setObjectName("coinjoin_tab")

        widget = QWidget()
        layout = QVBoxLayout(widget)
        self.other_pools_widget = QWidget()
        pools_layout = QVBoxLayout(self.other_pools_widget)

        form_layout = QFormLayout()

        label = QLabel("Enter a new address:")
        self.address_text_fields[wallet] = QLineEdit()
        self.address_text_fields[wallet].setFixedWidth(500)

        submit_button = QPushButton("Register")
        submit_button.setFixedWidth(100)

        create_pool_button = QPushButton("Create new pool")
        create_pool_button.setFixedWidth(200)

        my_pool_button = QPushButton("My pools")
        my_pool_button.setFixedWidth(200)

        view_pool_button = QPushButton("View other pools")
        view_pool_button.setFixedWidth(200)

        """
        My pools
        """
        self.wallet = wallet
        self.my_pools = self.mypools()

        """
        Other pools
        """
        event_type = "new_pool"
        event_kind = 2022

        self.other_pools = self.get_valid_pools(self.getevents(event_type, event_kind=event_kind)[0])

        create_pool_button.clicked.connect(
            lambda: self.show_create_pool_form(tab_widget)
        )
        my_pool_button.clicked.connect(
            lambda: self.show_pools_form(
                tab_widget, self.my_pools, join=False
            )
        )

        def refresh_and_update_pools():
            self.other_pools = self.get_valid_pools(self.getevents(event_type, event_kind=event_kind)[0])
            self.show_pools_form(
                tab_widget,
                self.other_pools,
                join=True
            )

        view_pool_button.clicked.connect(
            lambda: refresh_and_update_pools()
        )

        self.other_pools_widget = QWidget()
        pools_layout = QVBoxLayout(self.other_pools_widget)

        pools_layout.addWidget(create_pool_button)
        pools_layout.addWidget(my_pool_button)
        pools_layout.addWidget(view_pool_button)

        field_button_layout = QHBoxLayout()
        field_button_layout.addWidget(self.address_text_fields[wallet])
        field_button_layout.addWidget(submit_button)

        form_layout.addRow(label, field_button_layout)

        layout.addStretch(1)
        layout.addLayout(form_layout)
        layout.addStretch(1)

        tab_widget.addTab(self.other_pools_widget, _("Pools"))

        window.tabs.addTab(tab_widget, _("Coinjoin"))

        self.signal_emitter = SignalEmitter()

        self.signal_emitter.register_output_signal.connect(self.register_output)
        self.signal_emitter.show_inputlist_signal.connect(self.show_inputlist)
        self.signal_emitter.add_tab_signal.connect(self.add_tab)
        self.signal_emitter.update_finalized_signal.connect(self.update_finalized)

    def mypools(self):
        wallet_directory = self.wallet.storage.path
        base_directory = os.path.dirname(os.path.dirname(wallet_directory))
        pool_path = os.path.join(base_directory, "pools.json")
        my_pools_data = []

        if os.path.exists(pool_path) and os.stat(pool_path).st_size > 0:
            with open(pool_path, "r") as file:
                try:
                    my_pools_data = json.load(file)
                    my_pools_data.reverse()
                except Exception as e:
                    print(f"Error loading pools from JSON: {e}")

        return my_pools_data

    def show_create_pool_form(self, tab_widget):
        tab_layout = tab_widget.widget(0).layout()

        if hasattr(self, "form_groupbox"):
            self.form_groupbox.setParent(None)

        create_pool_widget = QWidget()
        create_pool_layout = QVBoxLayout(create_pool_widget)

        denomination_label = QLabel("Denomination:")
        denomination_edit = QLineEdit()
        denomination_edit.setText(self.config.get("pool_denomination", ""))

        num_peers_label = QLabel("Number of peers:")
        num_peers_edit = QLineEdit()
        num_peers_edit.setText(self.config.get("num_peers", ""))

        create_button = QPushButton("Create")
        create_button.setFixedWidth(100)

        create_pool_layout.addWidget(denomination_label)
        create_pool_layout.addWidget(denomination_edit)
        create_pool_layout.addWidget(num_peers_label)
        create_pool_layout.addWidget(num_peers_edit)
        create_pool_layout.addWidget(create_button)

        self.form_groupbox = QGroupBox()
        self.form_groupbox.setLayout(create_pool_layout)

        tab_layout.addWidget(self.form_groupbox)

        new_address = self.wallet.get_unused_address()
        self.wallet.set_label(new_address, "Coinjoin output")
        self.address = new_address

        create_button.clicked.connect(
            lambda: self.create_channel(
                denomination=denomination_edit.text(),
                peers=num_peers_edit.text(),
                timeout=600,
                address=new_address,
                tab_widget=tab_widget,
            )
        )

    def show_pools_form(self, tab_widget, pools_data, join=None):
        tab_layout = tab_widget.widget(0).layout()

        if hasattr(self, "form_groupbox"):
            self.form_groupbox.setParent(None)

        my_pools_widget = QWidget()
        my_pools_layout = QVBoxLayout(my_pools_widget)

        pools_list_widget = QListWidget()
        for index, pool in enumerate(pools_data):
            try:
                pool_str = f"Denomination: {pool['denomination']}, Peers: {pool['peers']}, Relay: {pool['relay']}, Pubkey: {pool['public_key']}"
                pool_list_item = QListWidgetItem(pool_str)
                pools_list_widget.addItem(pool_list_item)
                if index == 0 and join == False:
                    pool_list_item.setBackground(QColor("yellow"))
                    pool_list_item.setForeground(QColor("black"))
            except Exception as e:
                print(f"Error processing pool item: {e}")

        if not pools_list_widget.count():
            no_pools_label = QLabel("No pools found")
            my_pools_layout.addWidget(no_pools_label)
            self.form_groupbox = QGroupBox()
            self.form_groupbox.setLayout(my_pools_layout)
            tab_layout.addWidget(self.form_groupbox)
            return

        join_button = QPushButton("Join")
        join_button.setFixedWidth(100)

        my_pools_layout.addWidget(pools_list_widget)
        if join == True:
            my_pools_layout.addWidget(join_button)

        self.form_groupbox = QGroupBox()
        self.form_groupbox.setLayout(my_pools_layout)

        tab_layout.addWidget(self.form_groupbox)

        join_button.clicked.connect(
            lambda: self.join_channel(
                request={"type": "join_pool"},
                pubkey=pools_data[pools_list_widget.currentRow()]["public_key"],
                relay=pools_data[pools_list_widget.currentRow()]["relay"],
                tab_widget=tab_widget,
                peers=pools_data[pools_list_widget.currentRow()]["peers"],
                denomination=pools_data[pools_list_widget.currentRow()]["denomination"],
            )
        )

    def create_channel(self, denomination, peers, timeout, address, tab_widget):
        letters = string.ascii_lowercase
        pool_id = "".join(random.choice(letters) for i in range(10)) + str(
            int(time.time())
        )

        private_key = PrivateKey()
        pub_key = private_key.public_key.hex()
        hour_fee = self.fetch_hour_fee()
        channel = {
            "type": "new_pool",
            "id": pool_id,
            "public_key": pub_key,
            "denomination": float(denomination),
            "peers": int(peers),
            "timeout": int(time.time()) + timeout,
            "relay": self.config.get("nostr_relay", 0),
            "fee_rate": hour_fee,
        }
        channel_creds = channel
        channel_creds["private_key"] = private_key.hex()

        wallet_directory = self.wallet.storage.path
        base_directory = os.path.dirname(os.path.dirname(wallet_directory))
        pool_path = os.path.join(base_directory, "pools.json")

        if os.path.exists(pool_path) and os.stat(pool_path).st_size > 0:
            self.my_pools = self.mypools()

        self.my_pools.append(channel_creds)

        with open(pool_path, "w") as file:
            json.dump(self.my_pools, file, indent=2)

        self.my_pools = self.mypools()

        self.pool = channel
        self.pool_amount = float(denomination)
        self.pool_fee_rate = hour_fee
        self.peers = int(peers)

        event = self.publish_event(channel, 2022)

        print(f"[joinstr plugin] New pool created. Event ID: {event.id}")
        QMessageBox.information(
            None, "Pools", f"New pool created\nEvent ID: {event.id}"
        )

        self.register_output(address, tab_widget, pub_key)

        thread = threading.Thread(
            target=self.run_checkevents,
            args=("pool_msg", "join_request", None, None, None, pub_key, self.peers),
        )
        thread.daemon = True
        thread.start()

    def join_channel(
        self, request, pubkey, relay, tab_widget, peers, denomination=None
    ):
        event = self.publish_event(request, 4, pubkey, relay=relay, qr=True)

        print(f"[joinstr plugin] Join request sent. Event ID: {event.id}")

        if not hasattr(self, "dialog"):
            self.dialog = QDialog()
            self.dialog.setWindowTitle("Pool Request")
            self.dialog_layout = QVBoxLayout()
            self.dialog.setLayout(self.dialog_layout)

            self.message_label = QLabel("Waiting for pool credentials...")
            self.message_label.setAlignment(Qt.AlignCenter)
            self.dialog_layout.addWidget(self.message_label)

            self.progress_bar = QProgressBar()
            self.progress_bar.setTextVisible(False)
            self.dialog_layout.addWidget(self.progress_bar)

            self.dialog.setFixedWidth(400)

            self.dialog.show()

        self.timer = QTimer()
        self.timer.timeout.connect(self.update_progress)
        self.timer.start(100)

        self.pool_amount = denomination

        self.signal_emitter.close_dialog_signal.connect(self.close_dialog_slot)

        self.peers = peers

        thread = threading.Thread(
            target=self.run_checkevents,
            args=(
                "own_msg",
                "credentials",
                tab_widget,
                None,
                None,
                event.public_key,
                peers,
            ),
        )
        thread.daemon = True
        thread.start()

    def update_progress(self):
        value = self.progress_bar.value()
        value += 1
        if value > 100:
            value = 0
        self.progress_bar.setValue(value)

    def decrypt_msg(self, event_msg, private_key_hex):

        private_key_hex = self.private_key_hex
        private_key_bytes = bytes.fromhex(private_key_hex)
        private_key = PrivateKey(private_key_bytes)

        msg = private_key.decrypt_message(event_msg.content, event_msg.public_key)

        return msg

    def share_credentials(self, msg, credentials, pubkey, relay):

        self.publish_event(credentials, 4, pubkey, relay=relay)

    def generate_qr_code(self, data):

        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=4,
        )
        qr.add_data(data)
        qr.make(fit=True)

        img_buffer = BytesIO()

        qr.make_image(fill_color="black", back_color="white").save(img_buffer)

        qr_pixmap = QPixmap()
        qr_pixmap.loadFromData(img_buffer.getvalue())

        return qr_pixmap

    def show_qr_code_dialog(self, data):

        qr_pixmap = self.generate_qr_code(data)

        dialog = QDialog()
        layout = QVBoxLayout()

        qr_label = QLabel()
        qr_label.setPixmap(qr_pixmap)
        layout.addWidget(qr_label)

        text_label = QLabel(
            f"Public key: {data}\n\n"
            "Please pay one-time fee for nostr relay so that above pubkey can read/write events\n "
            "Ignore if already paid or using a free relay. Close the QR to join the pool."
        )

        font = QFont()
        font.setPointSize(8)
        text_label.setFont(font)

        text_label.setAlignment(Qt.AlignCenter)
        layout.addWidget(text_label)

        dialog.setLayout(layout)
        dialog.exec_()

    def publish_event(
        self,
        data,
        kind,
        recipient_pubkey=None,
        relay=None,
        qr=None,
        pool=None,
    ):

        if relay == None:
            relay = self.config.get("nostr_relay", 0)

        relay_manager = RelayManager()
        relay_manager.add_relay(relay)

        time.sleep(1.25)

        if pool is True:
            self.private_key_hex = self.pool["private_key"]
            private_key_bytes = bytes.fromhex(self.private_key_hex)
            private_key = PrivateKey(private_key_bytes)
            recipient_pubkey = self.pool["public_key"]

        else:
            private_key = PrivateKey()

        if qr is True:
            self.show_qr_code_dialog(private_key.public_key.hex())
            self.private_key_hex = private_key.hex()

        if kind == 4:
            event = EncryptedDirectMessage(
                recipient_pubkey=recipient_pubkey, cleartext_content=str(data)
            )
        else:
            event = Event(
                content=json.dumps(data),
                public_key=private_key.public_key.hex(),
                kind=kind,
            )

        private_key.sign_event(event)
        relay_manager.publish_event(event)
        time.sleep(2)

        relay_manager.close_all_relay_connections()

        return event

    def getevents(self, event_type, event_kind=None, pubkey=None):

        letters = string.ascii_lowercase
        random_id = "".join(random.choice(letters) for i in range(10))

        self.saved_relay = self.config.get("nostr_relay", 0)

        filters = Filters(
            [Filter(pubkey_refs=[pubkey], kinds=[event_kind])]
            if pubkey is not None
            else [Filter(kinds=[event_kind])]
        )
        subscription_id = random_id
        request = [ClientMessageType.REQUEST, subscription_id]
        request.extend(filters.to_json_array())

        relay_manager = RelayManager()
        relay_manager.add_relay(self.saved_relay)
        relay_manager.add_subscription_on_all_relays(subscription_id, filters)
        time.sleep(1.25)

        pool_list = []
        pool_msgs = []
        own_msgs = []

        i = 0

        while relay_manager.message_pool.has_events():
            event_msg = relay_manager.message_pool.get_event()
            event = (
                json.loads(event_msg.event.content)
                if event_kind != 4
                else event_msg.event
            )
            if event_type == "new_pool":
                try:
                    pool_data = {
                        "id": event["id"],
                        "public_key": event["public_key"],
                        "denomination": event["denomination"],
                        "peers": event["peers"],
                        "timeout": event["timeout"],
                        "relay": event["relay"],
                    }
                    pool_list.append(pool_data)
                except:
                    continue
            elif event_type == "pool_msg":
                try:
                    pool_msgs.append(event)
                except:
                    continue
            elif event_type == "own_msg":
                try:
                    own_msgs.append(event)
                except:
                    continue

            i = i + 1

        relay_manager.close_subscription_on_all_relays(subscription_id)
        relay_manager.close_all_relay_connections()

        return pool_list, pool_msgs, own_msgs

    def reset(self, tab_widget):
        if self.spinner is not None:
            self.spinner.stop()
        self.address = None
        self.private_key_hex = None
        self.output_list = []
        self.input_list = []
        self.peers = None
        self.pool = None
        self.pool_amount = None
        self.pool_fee_rate = None
        tab_widget.removeTab(1)
        self.input_registration_widget = None
        tab_widget.removeTab(1)
        self.finalization_widget = None
        tab_widget.setCurrentWidget(self.other_pools_widget)

    def update_finalized(self, tab_widget, finalization_widget, label):
        layout = finalization_widget.layout()
        label.setVisible(False)
        self.spinner.stop()
        self.spinner_widget.setVisible(False)
        completion_label = QLabel("Coinjoin broadcasted successfully.")
        completion_label.setAlignment(Qt.AlignCenter)
        layout.addWidget(completion_label)
        reset_button = QPushButton("Reset")
        reset_button.setFixedWidth(200)
        reset_button.clicked.connect(
            lambda: self.reset(tab_widget)
        )
        layout.addWidget(reset_button)
        layout.setAlignment(reset_button, Qt.AlignCenter)
        tab_widget.setCurrentWidget(self.finalization_widget)

    def show_inputlist(self, tab_widget, input_registration_widget, label):
        layout = input_registration_widget.layout()

        label.setVisible(False)
        self.reset_button.setVisible(False)

        select_label = QLabel("Select an input from the list:")
        select_label.setAlignment(Qt.AlignCenter)
        layout.addWidget(select_label)

        input_list = QListWidget()

        for utxo in self.wallet.get_utxos():
            item = QListWidgetItem(
                str(utxo.prevout.txid.hex() + ":" + str(utxo.prevout.out_idx))
            )
            item.setData(Qt.UserRole, utxo)
            input_list.addItem(item)
        layout.addWidget(input_list)

        register_button = QPushButton("Register")
        layout.addWidget(register_button)
        register_button.setFixedWidth(100)
        layout.setAlignment(register_button, Qt.AlignCenter)

        reset_button = QPushButton("Reset")
        layout.addWidget(reset_button)
        reset_button.setFixedWidth(100)
        reset_button.clicked.connect(
            lambda: self.reset(tab_widget)
        )
        layout.setAlignment(reset_button, Qt.AlignCenter)

        def on_register_button_clicked():
            register_button.setDisabled(True)
            selected_items = input_list.selectedItems()
            if selected_items:
                selected_item = selected_items[0]
                selected_input = selected_item.data(Qt.UserRole)
                self.register_input(
                    selected_input, self.pool_amount, tab_widget, register_button, self.peers
                )

        register_button.clicked.connect(on_register_button_clicked)
        self.spinner.stop()
        self.spinner = None
        self.spinner_widget = None
        tab_widget.setCurrentWidget(input_registration_widget)

    def add_tab(self, tab_widget, widget, label):
        tab_widget.addTab(widget, label)

    def update_input_registration(self, tab_widget, tab_label):
        tab_widget.addTab(self.input_registration_widget, tab_label)
        tab_widget.setCurrentWidget(self.input_registration_widget)
        self.spinner.start()

    def update_finalization(self, tab_widget, tab_label):
        tab_widget.addTab(self.finalization_widget, tab_label)
        tab_widget.setCurrentWidget(self.finalization_widget)
        self.spinner.start()

    def run_checkevents(
        self,
        event_type,
        sub_type,
        tab_widget=None,
        input_registration_widget=None,
        label=None,
        pubkey=None,
        peers=None,
    ):
        if event_type == "own_msg" and sub_type == "credentials":
            own_msgs = []
            while not own_msgs:
                _, _, own_msgs = self.getevents("own_msg", 4, pubkey=pubkey)
                time.sleep(30)

            encrypted_msg = max(own_msgs, key=lambda x: x.created_at)

            """
            Decrypt the message and save the credentials in pools.json
            """

            channel_creds = self.decrypt_msg(encrypted_msg, self.private_key_hex)

            if isinstance(channel_creds, str):
                try:
                    channel_creds = ast.literal_eval(channel_creds)
                except ValueError:
                    print("Error converting string to dictionary")

            self.signal_emitter.close_dialog_signal.emit()

            wallet_directory = self.wallet.storage.path
            base_directory = os.path.dirname(os.path.dirname(wallet_directory))
            pool_path = os.path.join(base_directory, "pools.json")

            self.my_pools.append(channel_creds)

            with open(pool_path, "w") as file:
                json.dump(self.my_pools, file, indent=2)

            self.my_pools = self.mypools()
            address = self.wallet.get_unused_address()
            self.address = address
            self.pool = self.my_pools[0]
            self.pool_fee_rate = self.pool["fee_rate"]
            '''
            TODO: validate this against our locally fetched fee rate
            '''

            self.signal_emitter.register_output_signal.emit(
                address, tab_widget, self.pool["public_key"], True
            )

        elif event_type == "pool_msg" and sub_type == "join_request":
            credentials = {
                "id": self.pool["id"],
                "public_key": self.pool["public_key"],
                "denomination": self.pool["denomination"],
                "peers": self.pool["peers"],
                "timeout": self.pool["timeout"],
                "relay": self.pool["relay"],
                "private_key": self.pool["private_key"],
                "fee_rate": self.pool["fee_rate"],
            }

            decrypted_msg_info = []

            while True:
                time.sleep(30)
                _, pool_msgs, _ = self.getevents("pool_msg", 4, pubkey=pubkey)
                for msg in pool_msgs:
                    if self.pool is not None and "private_key" in self.pool:
                        decrypted_content = self.decrypt_msg(msg, self.pool["private_key"])
                        if "{'type': 'join_pool'}" in decrypted_content:
                            if decrypted_content not in decrypted_msg_info:
                                decrypted_msg_info.append(
                                    (decrypted_content, msg.public_key)
                                )

                if len(decrypted_msg_info) == int(peers) - 1:
                    break

            if decrypted_msg_info:

                last_decrypted_content, last_public_key = decrypted_msg_info[-1]
                last_decrypted_content = last_decrypted_content.replace("'", '"')
                last_decrypted_msg = json.loads(last_decrypted_content)

                self.share_credentials(
                    last_decrypted_msg,
                    credentials,
                    last_public_key,
                    self.pool["relay"],
                )

        elif event_type == "pool_msg" and sub_type == "output":
            while True:
                time.sleep(30)
                _, pool_msgs, _ = self.getevents("pool_msg", 4, pubkey=pubkey)

                for msg in pool_msgs:
                    decrypted_content = self.decrypt_msg(msg, self.pool["private_key"])
                    if "\"type\": \"output\"" in decrypted_content:
                        parsed_content = json.loads(decrypted_content)
                        if parsed_content["address"] not in self.output_list:
                            self.output_list.append(parsed_content["address"])

                if len(self.output_list) == int(peers):
                    break
            self.signal_emitter.show_inputlist_signal.emit(
                tab_widget, self.input_registration_widget, label
            )
        elif event_type == "pool_msg" and sub_type == "input":
            while True:
                time.sleep(30)
                _, pool_msgs, _ = self.getevents("pool_msg", 4, pubkey=pubkey)

                for msg in pool_msgs:
                    decrypted_content = self.decrypt_msg(msg, self.pool["private_key"])
                    if "\"type\": \"input\"" in decrypted_content:
                        parsed_content = json.loads(decrypted_content)
                        if parsed_content["hex"] not in self.input_list:
                            self.input_list.append(parsed_content["hex"])

                if len(self.input_list) == int(peers):
                    break

            final_tx = self.finalize_coinjoin()
            self.broadcast_transaction(final_tx, retries=3)
            self.signal_emitter.update_finalized_signal.emit(
                tab_widget, self.finalization_widget, label
            )

    def register_output(
        self, address, tab_widget=None, pub_key=None, join=None
    ):
        event_type = "pool_msg"
        sub_type = "output"

        data = {"address": address, "type": "output"}
        data = json.dumps(data)

        event = self.publish_event(
            data, 4, pub_key, relay=self.pool["relay"], pool=True
        )
        print(f"[joinstr plugin] Output registered for coinjoin. Event ID: {event.id}")

        self.input_registration_widget = QWidget()
        layout = QVBoxLayout(self.input_registration_widget)
        label = QLabel("Waiting for other users to register outputs...")
        label.setAlignment(Qt.AlignCenter)
        layout.addWidget(label)
        self.spinner_widget = QWidget()
        self.spinner = WaitingSpinner(
            self.spinner_widget,
            roundness=100.0,
            fade=80.0,
            radius=10,
            lines=20,
            line_length=10,
            line_width=2,
            speed=1.5707963267948966,
            color=QColor(95, 174, 230),
        )
        layout.addWidget(self.spinner_widget)

        self.reset_button = QPushButton("Reset")
        self.reset_button.setFixedWidth(200)
        self.reset_button.clicked.connect(
            lambda: self.reset(tab_widget)
        )
        layout.addWidget(self.reset_button)
        layout.setAlignment(self.reset_button, Qt.AlignCenter)

        tab_label = "Input Registration"
        self.update_input_registration(tab_widget, tab_label)

        thread = threading.Thread(
            target=self.run_checkevents,
            args=(
                event_type,
                sub_type,
                tab_widget,
                self.input_registration_widget,
                label,
                pub_key,
                self.peers,
            ),
        )
        thread.daemon = True
        thread.start()

    def register_input(
        self, selected_input, pool_amount, tab_widget, register_button, peers=None
    ):
        """
        Check if UTXO used for input has the correct amount for pool
        """

        if not (
            (pool_amount * 100000000) + 500
            <= selected_input.value_sats()
            <= (pool_amount * 100000000) + 5000
        ):

            print(
                f"Error: Selected input value is not within the specified range for this pool (denomination: {pool_amount * 100000000}), selected input: {selected_input}"
            )
            register_button.setDisabled(False)
            return

        event_type = "pool_msg"
        sub_type = "input"

        sighash_type = 0x01 | 0x80

        """
        Create PSBT with our input and all registered outputs
        """
        prevout = selected_input.prevout
        txin = PartialTxInput(prevout=prevout)
        txin._trusted_value_sats = selected_input.value_sats()

        outputs = []
        output_ct = len(self.output_list)

        pool_amount_sats = int(pool_amount * 100000000)
        estimated_vbyte_size = 100 * output_ct
        estimated_fee = int(self.pool_fee_rate * estimated_vbyte_size)
        self.output_amount = pool_amount_sats - estimated_fee

        txout = [
            PartialTxOutput.from_address_and_value(address, int(amount_btc))
            for address, amount_btc in outputs
        ]

        self.tx = PartialTransaction.from_io([txin], txout)

        for txin in self.tx.inputs():
            txin.sighash = sighash_type

        signed_tx = self.wallet.sign_transaction(self.tx, None, ignore_warnings=True)

        serialized_psbt = signed_tx.serialize_as_bytes(force_psbt=True)
        base64_psbt = base64.b64encode(serialized_psbt).decode("ascii")

        data = {"hex": str(base64_psbt), "type": "input"}
        data = json.dumps(data)
        kind = 4
        event = self.publish_event(data, kind, pool=True)

        print(
            f"[joinstr plugin] Signed input registered for coinjoin. Event ID: {event.id}"
        )
        QMessageBox.information(
            None,
            "Input Registration",
            f"Signed input registered for coinjoin\nEvent ID: {event.id}",
        )

        self.finalization_widget = QWidget()
        layout = QVBoxLayout(self.finalization_widget)
        finalization_label = QLabel("Waiting for other users to register signed inputs...")
        finalization_label.setAlignment(Qt.AlignCenter)
        layout.addWidget(finalization_label)
        self.spinner_widget = QWidget()
        self.spinner = WaitingSpinner(
            self.spinner_widget,
            roundness=100.0,
            fade=80.0,
            radius=10,
            lines=20,
            line_length=10,
            line_width=2,
            speed=1.5707963267948966,
            color=QColor(95, 174, 230),
        )
        layout.addWidget(self.spinner_widget)

        tab_label = "Finalization"
        self.update_finalization(tab_widget, tab_label)

        thread = threading.Thread(
            target=self.run_checkevents,
            args=(
                event_type,
                sub_type,
                tab_widget,
                self.finalization_widget,
                finalization_label,
                self.pool["public_key"],
                self.peers,
            ),
        )
        thread.daemon = True
        thread.start()

        return base64_psbt

    def fetch_hour_fee(self):
        url = "https://mempool.space/api/v1/fees/recommended"
        response = requests.get(url)
        if response.status_code == 200:
            data = response.json()
            return data.get('hourFee')
        return None

    def finalize_coinjoin(self):
        coinjoin_tx = PartialTransaction()

        our_output_present = False

        for psbt_64 in self.input_list:
            psbt = PartialTransaction.from_raw_psbt(psbt_64)
            coinjoin_tx.add_inputs(list(psbt.inputs()))
            existing_outputs = set(
                (out.address, out.value) for out in coinjoin_tx.outputs()
            )
            new_outputs = [
                out
                for out in psbt.outputs()
                if (out.address, out.value) not in existing_outputs
            ]
            coinjoin_tx.add_outputs(new_outputs)

            for out in new_outputs:
                if out.address == self.address and out.value == self.output_amount:
                    our_output_present = True

        if not our_output_present:
            print("Error: outputs failed to validate, our address is missing")
            return  

        print(f"[joinstr plugin] Coinjoin tx: {coinjoin_tx.serialize()}")

        return coinjoin_tx

    def broadcast_transaction(self, transaction, retries):
        if not isinstance(transaction, Transaction):
            print("Invalid transaction provided")
            return False
        network = Network.get_instance()
        if not network or not network.is_connected():
            print(f"Network not connected: {network}")
            return False

        def broadcast_done(future):
            if future.cancelled():
                print("Broadcast task was cancelled")
            elif future.exception():
                print(f"Error while broadcasting: {future.exception()}")
            else:
                result = future.result()
                if result:
                    print("Broadcast successful")
                else:
                    print("Broadcast failed")

        async def broadcast():
            attempt = 0
            while attempt <= retries:
                try:
                    result = await network.broadcast_transaction(transaction)
                    if result is None:
                        print(f"[joinstr plugin] Broadcasted coinjoin tx successfully")
                        return True
                    else:
                        error_msg = result.get('error') if result else 'No result returned'
                        print(f"Transaction broadcast failed: {error_msg}")
                except Exception as e:
                    print(f"Error while broadcasting: {e}")
                    if attempt == retries:
                        print("Broadcast failed after maximum retries")
                        return False
                attempt += 1
                if attempt <= retries:
                    print(f"Retrying broadcast... {retries - attempt + 1} retries left")
                    await asyncio.sleep(1)
            return False

        loop = asyncio.get_event_loop()
        task = loop.create_task(broadcast())
        task.add_done_callback(broadcast_done)
        print("Broadcast initiated...")