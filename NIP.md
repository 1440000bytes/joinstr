# NIP

## Joinstr: A protocol for coinjoin using nostr 

This NIP describes the nostr events used in joinstr for collaborative transactions to improve privacy on-chain.

## How does it work?

1. **Create a pool**: Alice can create a pool using basic encrypted channels with `kind:2022` event. Event contents have the below information about the pool:

```json
{
  "type": "new_pool",
  "id": <unique id>,
  "public_key": <new pubkey>,
  "denomination": <custom denomination>,
  "peers": <number of peers>,
  "timeout": <unix timestamp>,
  "relay": <comma separated list of relays>,
  "fee_rate": <fee rate for coinjoin tx>,
  "transport": "vpn/tor",
  "vpn_gateway": <riseup vpn gatewway address>
}
```

2. **View other pools and join**: Bob reads all `kind:2022` events and publishes a NIP 4 event with pool's pubkey being the recipient to join the pool. Pool responds with credentials and further communication happens in channel by publishing NIP 4 events with pool's pubkey as author and recipient.

## Details

- All events are published with a new pubkey
- Encrypted channel communication uses NIP 4 events sent to self
- All peers in a pool use the same IP address using Riseup VPN

```mermaid
sequenceDiagram
    participant Alice
    participant Encrypted Channel
    participant Nostr relay
    participant Bob

    Alice->>Encrypted Channel: Generate keypair for encrypted channel
    Encrypted Channel->>Nostr relay: Add channel pubkey
    Bob->>Encrypted Channel: Request channel credentials to join
    Bob->>Nostr relay: Add user pubkey
    Encrypted Channel->>Nostr relay: Respond with channel private key
    Bob->>Nostr relay: Read private key and join channel
```

### Pool Inititator

A participant initiates the coinjoin process by creating a pool, specifying the denomination and the number of peers. This initiator publishes their address within the pool and continuously monitors incoming messages. They provide necessary credentials to peers who send a join request. When all peers have joined the pool, input registration phase begins. PSBT is signed with SIGHASH_ALL | SIGHASH_ANYONECANPAY sighash flag for input registration.

### Pool Member

A peer who wants to join an existing pool sends a request for the credentials. Their client listens for messages sent to their public key, which was used for last NIP 4 event. Upon receiving the credentials, the peer joins the pool and publishes their address. Signed PSBT is shared in the input registration phase.

A transaction is created with the signed inputs, outputs and broadcasted to complete the coinjoin.

Outputs should be lexicographically sorted when creating signed PSBT for input registration and inputs should be sorted while joining all signed PSBTs.

## New Event Kinds

This NIP introduces one new event kind which is used for creating the pool.


```json
{
    "kind": 2022,
    "created_at": <unix timestamp in seconds>,
    "pubkey": <new pubkey>,
    "content": pool information as JSON,
    "tags": [],
    "sig": <signed with a new nostr pubkey> 
}
```
### JSON used for encrypted messages shared in the pool

```
{
    "address": "<address_value>",
    "type": "output"
}
```
```
{
    "psbt": "<base64_psbt_value>",
    "type": "input"
}
```

### Implementation

Electrum Plugin: https://gitlab.com/invincible-privacy/joinstr/-/tree/main/plugin/zip/
Kotlin: https://gitlab.com/invincible-privacy/joinstr-kmp

## Known trade-offs & future improvements

NIP 4 events will be replaced with better NIP to improve privacy

## Resources

Docs: https://docs.joinstr.xyz  
Sybil resistance: https://github.com/AdamISZ/aut-ct
