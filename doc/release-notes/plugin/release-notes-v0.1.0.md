Joinstr plugin v0.1.0 for electrum: https://gitlab.com/invincible-privacy/joinstr/-/blob/main/plugin/zip/v0.1.0/joinstr.zip

Please report bugs using the issue tracker at Gitlab: https://gitlab.com/invincible-privacy/joinstr/-/issues

This is a pre-alpha release which can be used on mainnet with some trade-offs.

How to Use
==============

- Install all dependencies
- Unzip release and copy joinstr plugin in `/electrum/electrum/plugins` directory
- Launch Electrum. Go to plugin settings and enable joinstr. Set relay, denomination and peers.
- Re-launch Electrum and start using plugin

Tutorial: [https://uncensoredtech.substack.com/p/tutorial-electrum-plugin-for-joinstr](https://uncensoredtech.substack.com/p/tutorial-electrum-plugin-for-joinstr)

Changes
===============

Pools using encrypted channels: Electrum plugin is now using pools using nostr encrypted channels. Users can create or join existing
pools for coinjoin.

Riseup VPN: Riseup VPN is used which connects to a server based on [config](https://gitlab.com/invincible-privacy/joinstr/-/blob/main/config.vpn).
This ensures all the peers in a pool use the same IP address to connect with relays and publish nostr events.

Use of ANYONECANPAY sighash: All participants sign PSBT using SIGHASH_ALL | SIGHASH_ANYONECANPAY flag in the input registration phase.

Support for paid nostr relays: Paid nostr relays can be used for pools in which members need to pay a one-time fee to join the pool.

Credits
=======

djkazic  
pythcoiner  
fiatjaf  
jeffthibault  
ecdsa  

Thanks to BOB Space and everyone who contributed to https://geyser.fund/project/joinstr