import requests
import time
import json
from nostr_sdk import Keys, Client, EventBuilder, Filter

url = "http://127.0.0.1:<PORT>/wallet/<WALLET_NAME>"

'''
Change RPC credentials
'''

headers = {
    'Authorization': 'Basic dXNlcjpwYXNz',
    'Content-Type': 'text/plain'
}


def publish():
    
    keys = Keys.generate()
    kind = 2022
    content = json.dumps(data) 
    tags = [] 

    builder = EventBuilder(kind, content, tags)
    event = builder.to_event(keys)

    client = Client(keys)
    client.add_relay("wss://relay.nostr.info")
    client.connect() 

    event_id = client.send_event(event)

    return event_id.to_hex()


def get_events():

    client = Client()
    client.add_relay("wss://relay.nostr.info")
    client.connect()

    filter = Filter().kinds([2022])
    events = client.get_events_of([filter], None)

    desc_list = []
    output_list = []
    upsbt = []
    spsbt = []

    i = 0

    for event in events:
        content = json.loads(event.content)
        if event_type == "input":
            try:
                desc_list.append(content['descriptor'])
            except:
                continue
        elif event_type == "output":
            try:
                output_list.append(content['address'])
            except:
                continue
        elif event_type == "unsigned":
            try:
                upsbt.append(content['unsigned_psbt'])
            except:
                continue
        elif event_type == "signed":
            try:
                spsbt.append(content['signed_psbt'])
            except:
                continue

        i = i + 1

    client.disconnect()

    return content, output_list, desc_list, upsbt, spsbt, i


def check_events(event_type):
    time.sleep(30)
    event, output_list, desc_list, upsbt, spsbt, num_e = get_events(event_type)

    if num_e % 5 != 0 or num_e == 0:
        check_events(event_type)
    else:
        if event_type == 'input':
            round_desc_list = []
            for k in range(len(desc_list) - 5, len(desc_list)):
                if desc_list[k] not in round_desc_list:
                    round_desc_list.append(desc_list[k])
            return round_desc_list, num_e
        elif event_type == 'output':
            round_output_list = []
            for k in range(len(output_list) - 5, len(output_list)):
                if output_list[k] not in round_output_list:
                    round_output_list.append(output_list[k])
            return round_output_list, num_e
        elif event_type == 'signed':
            round_spsbt_list = []
            for k in range(len(spsbt) - 5, len(spsbt)):
                if spsbt[k] not in round_spsbt_list:
                    round_spsbt_list.append(spsbt[k])
            return round_spsbt_list, num_e


def list_unspent():

    payload = "{\"jsonrpc\": \"1.0\", \"id\": \"joinstr\", \"method\": \"listunspent\"}"
    response = requests.request("POST", url, headers=headers, data=payload)

    for i in range(0, len(response.json()['result'])):
        desc = response.json()['result'][i]['desc']
        desc_list.append(str(desc))
    return desc_list


def get_address():
    payload = "{\"jsonrpc\": \"1.0\", \"id\": \"joinstr\", \"method\": \"getnewaddress\"}"
    response = requests.request("POST", url, headers=headers, data=payload)

    return response.json()['result']


def create_tx():

    payload = "{\"jsonrpc\": \"1.0\",\r\n \"id\": \"joinstr\",\r\n  \"method\": \"createpsbt\",\r\n  \"params\": [[{\"txid\":\"" + str(round_tx_list[0]) + "\",\"vout\":" + str(round_vout_list[0]) + "},{\"txid\":\"" + str(round_tx_list[1]) + "\",\"vout\":" + str(round_vout_list[1]) + "}, {\"txid\":\"" + str(round_tx_list[2]) + "\",\"vout\":" + str(round_vout_list[2]) + "},{\"txid\":\"" + str(round_tx_list[3]) + "\",\"vout\":" + str(round_vout_list[3]) + "},{\"txid\":\"" + str(round_tx_list[4]) + "\",\"vout\":" + str(round_vout_list[4]) + "}],\r\n    [{\"" + str(round_output_list[0]) +"\":" + str(postmix_ov) + "},\r\n    {\"" + str(round_output_list[1]) + "\":" + str(postmix_ov) + "},\r\n    {\"" + str(round_output_list[2]) + "\":" + str(postmix_ov) + "},\r\n    {\"" + str(round_output_list[3]) + "\":" + str(postmix_ov) + "},\r\n    {\"" + str(round_output_list[4]) + "\":" + str(postmix_ov) + "}]]\r\n}"
    response = requests.request("POST", url, headers=headers, data=payload)

    upsbt = response.json()['result']

    return upsbt


def get_utxo_info():

    payload = "{\"jsonrpc\": \"1.0\",\r\n \"id\": \"joinstr\",\r\n  \"method\": \"scantxoutset\",\r\n  \"params\": [\"start\", [\"" + str(round_desc_list[j]) + "\"]]\r\n}"
    response = requests.request("POST", url, headers=headers, data=payload)

    txid = response.json()['result']['unspents'][0]['txid']
    vout = response.json()['result']['unspents'][0]['vout']
    amount = response.json()['result']['total_amount']

    return txid, vout, amount


def sign_tx(upsbt):

    payload = "{\"jsonrpc\": \"1.0\",\r\n \"id\": \"joinstr\",\r\n  \"method\": \"walletprocesspsbt\",\r\n  \"params\": [\"" + str(upsbt) + "\"]\r\n}"
    response = requests.request("POST", url, headers=headers, data=payload)

    signed_psbt = response.json()['result']['psbt']

    return signed_psbt


def combine_tx():

    payload = "{\"jsonrpc\": \"1.0\",\r\n \"id\": \"joinstr\",\r\n  \"method\": \"combinepsbt\",\r\n  \"params\": [[\"" + str(round_spsbt_list[0]) + "\",\"" + str(round_spsbt_list[1]) + "\",\"" + str(round_spsbt_list[2]) + "\",\"" + str(round_spsbt_list[3]) + "\",\"" + str(round_spsbt_list[4]) + "\"]]\r\n}"
    response = requests.request("POST", url, headers=headers, data=payload)

    combined_psbt = response.json()['result']

    return combined_psbt


def finalize_tx():

    payload = "{\"jsonrpc\": \"1.0\",\r\n \"id\": \"joinstr\",\r\n  \"method\": \"finalizepsbt\",\r\n  \"params\": [\"" + str(combined_tx) + "\"]\r\n}"
    response = requests.request("POST", url, headers=headers, data=payload)

    final_tx = response.json()['result']['hex']

    return final_tx


def broadcast():

    payload = "{\"jsonrpc\": \"1.0\",\r\n \"id\": \"joinstr\",\r\n  \"method\": \"sendrawtransaction\",\r\n  \"params\": [\"" + str(final_tx) + "\"]\r\n}"
    response = requests.request("POST", url, headers=headers, data=payload)

    release_txid = response.json()['result']

    return release_txid


if __name__ == "__main__":

    '''
    TODO: Review and test the while loops. Remove if not required.
    '''

    # Input registration

    print("List of utxos in wallet:\n")
    utxo_list = list_unspent()
    descriptor = input('\nEnter descriptor for the input registration: ')

    data = {"descriptor": descriptor, "type": "input"}

    event_id = publish()
    print("\nevent id: ", event_id)

    # Output registration

    while True:
        event_type = "input"
        try:
            round_desc_list, num_i = check_events('input')
        except:
            continue
        break

    event_type = "output"
    event, output_list, desc_list, upsbt, spsbt, num_o = get_events()

    last_bool = bool(num_o == 4 or (num_o - 4) % 5 == 0)

    address = get_address()

    data = {"address": address, "type": "output", "last": last_bool}

    event_id = publish()
    print("\n" + address + " registered for output")
    print("\nevent id:", event_id)

    '''
    TODO: Check outputs for `last_bool` before proceeding with next step
    '''

    while True:
        event_type = "output"
        try:
            round_output_list, num_o = check_events('output')
        except:
            continue
        break

    # Create and publish tx

    if last_bool is True:

        j = 0
        input_sum = 0
        round_tx_list = []
        round_vout_list = []

        while j < 5:
            round_tx, round_vout, amt = get_utxo_info()

            if 0.001 < amt < 0.0015:
                input_sum = input_sum + amt
                round_tx_list.append(round_tx)
                round_vout_list.append(round_vout)

                j = j+1
            else:
                print("Error: invalid input amount")
                break

        postmix_ov = (input_sum - 0.00001)/5

        upsbt = create_tx()

        print("\nUnsigned PSBT: " + upsbt)

    # Sign and publish tx

    event_type = "signed"
    event, output_list, desc_list, upsbt, spsbt, num_stx = get_events()

    last_bool = bool(num_stx == 4 or (num_stx - 4) % 5 == 0)

    data = {}
    data["signed_psbt"] = sign_tx(upsbt)
    data['last'] = last_bool

    spsbt = str(data["signed_psbt"])

    event_id = publish()

    print("\nSigned PSBT: " + str(spsbt))
    print("\nevent id:", event_id)

    # Combine, finalize and broadcast transaction

    '''
    TODO: Only do this step if we signed the PSBT last
    '''

    while True:
        event_type = "signed"
        try:
            round_spsbt_list, num_stx = check_events('signed')
        except:
            continue
        break

    combined_tx = combine_tx()
    final_tx = finalize_tx()

    data = broadcast()
    event_id = publish()

    print("\nCoinjoin tx: " + str(data))
    print("\nevent id:", event_id)
