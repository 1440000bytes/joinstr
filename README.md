# joinstr

### coinjoin implementation using nostr

![joinstr-new-logo][logo]

**Requirements:**

 - [python-nostr][python-nostr] for web app and electrum plugin
 - [nostr-sdk][nostr-sdk] for cli
 - Bitcoin Core for web app and cli

**Using electrum plugin**

```
$ pip install nostrj pyqtspinner pillow qrcode
```

- Copy plugin files in plugins directory
- Save nostr relay, denomination and number of peers in plugin settings
- Restart electrum and use the plugin for coinjoin

**Docs**: https://docs.joinstr.xyz

[logo]: https://i.imgur.com/UseTcdC.png
[python-nostr]: https://github.com/jeffthibault/python-nostr
[nostr-sdk]: https://pypi.org/project/nostr-sdk/
